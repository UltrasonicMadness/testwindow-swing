# How to use
* Compile the source file using the command `javac TestWindow.java`
* Run `java TestWindow`

# License
CC0
