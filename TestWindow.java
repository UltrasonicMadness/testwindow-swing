import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class TestWindow extends JFrame {
    public TestWindow() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        JLabel label = new JLabel("Test Label");
        JTextField textBox = new JTextField();
        JButton button = new JButton("Show dialog");

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                JOptionPane.showMessageDialog(TestWindow.this, "Text entered: " + textBox.getText(),
                        "Test dialog", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(label, constraints);
        constraints.weightx = 1;
        panel.add(textBox, constraints);
        constraints.weightx = 0;
        panel.add(button, constraints);
        this.add(panel);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(360, 240);
    }

    public static void main(String[] args) throws Exception {
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                new TestWindow().setVisible(true);
            }
        });
    }
}
